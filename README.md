# Focused Window Monitor

```bash
$ cd ~/.local/share/gnome-shell/extensions
$ git clone git@gitlab.com:dkx/gnome/focused-window-monitor.git focused-window-monitor@kudera.dev
$ gnome-extensions enable focused-window-monitor@kudera.dev
```
