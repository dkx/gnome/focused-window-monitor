'use strict';

import { Extension } from 'resource:///org/gnome/shell/extensions/extension.js';
import { FocusManager } from './focusManager.js';
import { DBus } from './dBus.js';
import { log } from './utils.js';


export default class FocusedWindowMonitorExtension extends Extension
{
	constructor(metadata)
	{
		super(metadata);
		this._focus = null;
		this._dBus = new DBus();
	}

	enable()
	{
		log('enabling...');
		this._dBus.start();
		this._focus = new FocusManager(title => {
			log(`new title: '${title}'`);
			this._dBus.updateTitle(title);
		});
	}

	disable()
	{
		log('disabling...');
		this._dBus.stop();
		if (this._focus !== null) {
			this._focus.destroy();
			this._focus = null;
		}
	}
}
