'use strict';

import Gio from 'gi://Gio';
import GLib from 'gi://GLib';
import { log } from './utils.js';


const INTERFACE_NAME = 'dev.kudera.FocusedWindow';
const PATH = '/dev/kudera/FocusedWindow';
const DBUS_SCHEMA = `
<node>
	<interface name="${INTERFACE_NAME}">
		<property name="CurrentTitle" type="s" access="read"/>
	</interface>
</node>`;

export class DBus
{
	constructor()
	{
		this._id = null;
		this._impl = null;
	}

	updateTitle(title)
	{
		if (this._impl !== null) {
			this._impl.updateTitle(title);
		}
	}

	start()
	{
		if (this._id !== null || this._impl !== null) {
			return;
		}

		log(Gio.bus_own_name.toString());

		this._id = Gio.bus_own_name(
			Gio.BusType.SESSION,
			INTERFACE_NAME,
			Gio.BusNameOwnerFlags.NONE,
			(connection, _name) => {
				this._impl = new DBusImpl();
				const exportedObject = Gio.DBusExportedObject.wrapJSObject(DBUS_SCHEMA, this._impl);
				this._impl._impl = exportedObject;
				exportedObject.export(connection, PATH);
			},
			() => {},
			() => {
				this._id = null;
				this._impl = null;
			}
		);
	}

	stop()
	{
		if (this._id === null) {
			return;
		}

		Gio.bus_unwatch_name(this._id);
		this._id = null;
		this._impl = null;
	}
}

class DBusImpl
{
	constructor()
	{
		this._impl = null;
		this._currentTitle = '';
	}

	get CurrentTitle()
	{
		return this._currentTitle;
	}

	updateTitle(title)
	{
		log(`emitting property...`);
		this._currentTitle = title;
		if (this._impl !== null) {
			log('emitting property changed...');
			try {
				this._impl.emit_property_changed('CurrentTitle', GLib.Variant.new_string(this._currentTitle));
			} catch (err) {
				log(`error: ${err.toString()}`);
				throw err;
			}
		}
	}
}
