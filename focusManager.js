'use strict';

export class FocusManager
{
	constructor(notifier)
	{
		this._notifier = notifier;
		this._currentWindow = null;
		this._focusChangedId = null;
		this._titleChangedId = null;
		this.currentTitle = '';

		this._focusChangedId = global.display.connect('notify::focus-window', this._onFocusChanged.bind(this));
		this._onFocusChanged();
	}

	destroy()
	{
		if (this._focusChangedId !== null) {
			global.display.disconnect(this._focusChangedId);
			this._focusChangedId = null;
		}

		this._disconnectTitleChangeSignal();
	}

	_onTitleChanged(window)
	{
		const newTitle = window === null ? '' : window.get_title();
		if (newTitle !== this.currentTitle) {
			this.currentTitle = newTitle;
			this._notifier(this.currentTitle);
		}
	}

	_onFocusChanged()
	{
		this._disconnectTitleChangeSignal();

		this._currentWindow = global.display.focus_window;
		if (this._currentWindow !== null) {
			this._titleChangedId = this._currentWindow.connect('notify::title', this._onTitleChanged.bind(this));
		}

		this._onTitleChanged(this._currentWindow);
	}

	_disconnectTitleChangeSignal()
	{
		if (this._titleChangedId !== null && this._currentWindow !== null) {
			this._currentWindow.disconnect(this._titleChangedId);
		}

		this._currentWindow = null;
		this._titleChangedId = null;
	}
}
