'use strict';

import GLib from 'gi://GLib';


let loggingEnabled = null;

export function log(msg)
{
	if (loggingEnabled === null) {
		loggingEnabled = GLib.getenv('FOCUSED_WINDOW_MONITOR_LOGGING') === 'true';
	}

	if (loggingEnabled === true) {
		console.log('[FOCUS]', msg);
	}
}
