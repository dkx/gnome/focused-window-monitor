#!/bin/sh -e

export FOCUSED_WINDOW_MONITOR_LOGGING=true
export MUTTER_DEBUG_DUMMY_MODE_SPECS=1366x768
#export G_MESSAGES_DEBUG=all
#export SHELL_DEBUG=all

dbus-run-session -- \
	gnome-shell --nested \
				--wayland
